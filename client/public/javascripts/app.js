'use strict';

(function(){
    var app = angular.module('appNotesClient', ['ngCookies','ngResource','ngSanitize','ngRoute','http-auth-interceptor','ngAnimate','ui.bootstrap']);

    app.config(function($routeProvider, $locationProvider) {
        /* Routing */
        $routeProvider
        .when('/login', {
            templateUrl: 'partials/login',
            controller: 'LoginCtrl'
        })
        .when('/signup', {
            templateUrl: 'partials/signup',
            controller: 'SignupCtrl'
        })
        .when('/notes', {
            templateUrl: 'partials/notes/list',
            controller: 'NotesCtrl'
        })
        .when('/notes/edit/:noteId', {
            templateUrl: 'partials/notes/edit',
            controller: 'NotesCtrl'
        })
        .otherwise({ redirectTo: '/'});
        $locationProvider.html5Mode(true);

    }).run(function ($rootScope, $location, $route, Auth) {
      $rootScope.$on('$routeChangeStart',
        function (event, next, current) {

            if( $location.path() != '/signup'){
                Auth.currentUser(function(user){
                    if (!user){
                      $location.path('/login');
                  }

              });
            }



            
        });
  });

})();
