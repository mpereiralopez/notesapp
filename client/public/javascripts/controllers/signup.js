'use strict';

angular.module('appNotesClient')
.controller('SignupCtrl', function ($scope, Auth, $location, $q) {

  $scope.errors = [];

  $scope.register = function(form) {
    $scope.errors = [];
    var userData = {
     email: $scope.user.email,
     username: $scope.user.username
   }

   var promise = check_new_userData(userData);
   promise.then(function(data){

    if(!data.exists_email && !data.exists_username){
      Auth.createUser({
        email: $scope.user.email,
        username: $scope.user.username,
        password: $scope.user.password
      },
      function(err) {
        if (!err) {
          $location.path('/');
        } else {
          angular.forEach(err.errors, function(error, field) {
            $scope.errors.push({message: 'Error with field '+error.type+", please check it"})
          });
        }
      }
      
      );
    }else{
      if(data.exists_email)$scope.errors.push({message:'An user already exist with this email, please choose other.'});
      if(data.exists_username)$scope.errors.push({message:'An user already exist with this user name, please choose other.'});
    }

  }).catch(function(){
    $scope.errors.push({message:'Unknown error during singup process, please try later.'});
  });

};

/* Auxiliar function for check email availability */
function check_new_userData(userData){
  return $q(function(resolve, reject) {
    Auth.check_new_user_data(userData).then(function(data){
      resolve (data);
    });


  });

}

});