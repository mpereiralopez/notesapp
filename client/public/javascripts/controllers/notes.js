'use strict';

angular.module('appNotesClient')
.controller('NotesCtrl', function ($scope, Notes, $location, $route,$routeParams, $rootScope) {

  var actualNoteId = $routeParams.noteId;
  $scope.acutalNote = null;
  $scope.notes = [];

  $scope.edit = function() {

    var version = ($scope.acutalNote.version!=null)?$scope.acutalNote.version+1:1

    var note = new Notes({
      subject: $scope.acutalNote.subject,
      body: $scope.acutalNote.body,
      UserId:$scope.$parent.currentUser.id,
      version:version
    });

    note.$save(function(response) {
      $location.path("/notes");
    });

  };

  $scope.remove = function(noteId) {
    var r = confirm("Are you sure you want to delete the note?");
    if (r == true) {
      Notes.get({
        noteId: noteId
      }, function(note) {
        note.$remove({
          noteId: noteId
        },function(){
          if($location.path() == '/notes'){
            $route.reload();
          }else{
           $location.path("/notes");
         }
       });
        
      });
    } 
  };

  $scope.find = function() {
    Notes.query(function(notes) {
      $scope.notes = notes;
    },function(err){
      console.log("err");
    });
  };

  $scope.findOne = function() {
    Notes.get({
      noteId: $routeParams.noteId
    }, function(note) {
      $scope.acutalNote = (note.id)?note:null;
    });
  };
  if (actualNoteId)$scope.findOne ();
});
