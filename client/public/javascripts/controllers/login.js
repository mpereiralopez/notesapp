'use strict';

angular.module('appNotesClient')
  .controller('LoginCtrl', function ($scope, Auth, $location) {
    $scope.UnregisterUser = {};
    $scope.errors = [];
    $scope.login = function(form) {
      Auth.login({
          'email':  $scope.unregisterUser.email,
          'password': $scope.unregisterUser.password
        }).then(function(user){
            $scope.user = user;
            $location.path('/notes');
        }).catch(function(){
          $scope.errors = [];
          $scope.errors.push({message:'Error on login, please check your credentials.'});
        });
    }
  });