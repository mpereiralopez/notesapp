'use strict';

angular.module('appNotesClient')
.controller('NavbarCtrl', function ($scope, Auth, $location) {
  $scope.menu = [{
    "title": "Notes",
    "link": "notes"
  }];

  $scope.authMenu = [{
    "title": "Create New Note",
    "link": "notes/edit/-1"
  }];

  $scope.status = {
    isopen: false
  };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

  $scope.logout = function() {
    Auth.logout(function(err) {
      if(!err) {
        $location.path('/login');
      }
    });
  };
});