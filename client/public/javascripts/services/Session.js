'use strict';

angular.module('appNotesClient')
.factory('Session', function ($resource) {
	return $resource('/auth/session/');
});