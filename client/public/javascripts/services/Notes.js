'use strict';

angular.module('appNotesClient')
  .factory('Notes', function ($resource) {
    return $resource('api/notes/:noteId', {
      noteId: '@_id'
    }, {
      remove: {
        method: 'DELETE'
      }
    });
  });
