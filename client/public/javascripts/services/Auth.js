'use strict';

angular.module('appNotesClient')
.factory('Auth', function Auth($location, $rootScope, Session, User, $cookieStore,$http,$q) {

 // console.log($cookieStore.get('user'));
  //$rootScope.currentUser = $cookieStore.get('user') || null;
  $rootScope.currentUser = null;
  $cookieStore.remove('user');

  return {


    check_new_user_data: function (userData){
       // create a new instance of deferred
       var deferred = $q.defer();

        // send a post request to the server
        $http.get('/auth/check/', 
        {
          params:{
            email: userData.email, 
            username: userData.username
          }
        })
        // handle success
        .then(function (data) {
          if(data.status === 200 && data.data){
            deferred.resolve(data.data);
          } else {
            deferred.reject();
          }
        })
        // handle error
        .catch(function (data) {
          deferred.reject();
        });

        // return promise object
        return deferred.promise;
      },

      login: function(user) {
         // create a new instance of deferred
         var deferred = $q.defer();

        // send a post request to the server
        $http.post('/auth/session/', {email: user.email, password: user.password})
        // handle success
        .then(function (data) {
          if(data.status === 200 && data.data.id){
            user = true;
            $rootScope.currentUser = data.data;
            deferred.resolve(data.data);
          } else {
            user = false;
            deferred.reject();
          }
        })
        // handle error
        .catch(function (data) {
          user = false;
          deferred.reject();
          
        });

  // return promise object
  return deferred.promise;
},

logout: function(callback) {
  var cb = callback || angular.noop;
  Session.delete(function(res) {
    $rootScope.currentUser = null;
    return cb();
  },
  function(err) {
    return cb(err.data);
  });
},

createUser: function(userinfo, callback) {
  var cb = callback || angular.noop;
  User.save(userinfo,
    function(user) {
      $rootScope.currentUser = user;
      return cb();
    },
    function(err) {
      return cb(err.data);
    });
},

currentUser: function(callback) {
  var cb = callback || angular.noop;
  $http.get('/auth/session/')
  // handle success
  .then(function (data) {
    if(data.data && data.data.user){
      $rootScope.currentUser = data.data.user;
    } else {
      $rootScope.currentUser = null;
    }
    return cb($rootScope.currentUser);
  })
  // handle error
  .catch(function (data) {
    return cb(null);
  });
}


};
})