'use strict';
var noteController = require('../controllers/notes');
/**
 *  Route middleware to ensure user is authenticated.
 */
exports.ensureAuthenticated = function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.status(200).json(null);
}

/**
 * Notes authorizations routing middleware
 */



 exports.hasAuthorization= function(req, res ,next) {
  noteController.getNote(req.params.noteId).then(function(note){
   if (note.UserId.toString() !== req.user.id.toString()) {
    return res.send(403);
  }
  next();
});

  
};