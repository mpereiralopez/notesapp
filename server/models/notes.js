var User = require('./user');

module.exports = function(sequelize, DataTypes) {


  var Note = sequelize.define('Note', {
    subject: {type: DataTypes.STRING, allowNull: false, notEmpty: true},
    body: {type: DataTypes.TEXT, allowNull: false, notEmpty: true},
    version: {type: DataTypes.INTEGER, allowNull: false, notEmpty: true},
    status: { type: DataTypes.STRING, defaultValue: 'active' }

  },
  {

    classMethods: {
      associate: function(models) {
        Note.belongsTo(models.User, {
          onDelete: "CASCADE",
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  }
  );
  return Note
}
