var bcrypt = require('bcrypt-nodejs')

module.exports = function(sequelize, DataTypes) {

  var  SALT_WORK_FACTOR = 10;


  var User = sequelize.define('User', {
    username: {type: DataTypes.STRING, unique: true, allowNull: false, notEmpty: true},
    password: {type: DataTypes.STRING, allowNull: false, notEmpty: true},
    email: {type: DataTypes.STRING, unique: true, allowNull: false, notEmpty: true}
  },
  {


    hooks: {
      beforeCreate : function(user, options, next) {
        bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
          bcrypt.hash(user.password, salt, function(progress){} ,function(err, hash) {
            user.password = hash;
            next(null, user);
          });
        });
      }
    },

    classMethods: {
      associate: function(models) {
        User.hasMany(models.Note)
      }
    },

    instanceMethods: {
      validPassword: function(password) {
        return bcrypt.compareSync(password, this.password);
      }
    }
  }
);
 return User  
}