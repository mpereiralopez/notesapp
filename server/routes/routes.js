'use strict';

var path = require('path'),
auth = require('../config/auth');

var express = require('express');
var router = express.Router();
var passport = require('passport');
var session = require('../controllers/session');
var users = require('../controllers/users');
var notes = require('../controllers/notes');


module.exports = function(router) {


  // Angular Routes*/
  router.get('/auth/session', auth.ensureAuthenticated, session.session);
  router.post('/auth/session', session.login);
  router.delete('/auth/session', session.logout);


 // User Routes
 router.post('/auth/users', users.create);
 router.get('/auth/check/',users.exists);




  // Note Routes
  router.get('/api/notes', notes.all);
  router.post('/api/notes', auth.ensureAuthenticated, notes.create);

  router.route('/api/notes/:noteId')
  .get(function(req,res){

    auth.ensureAuthenticated(req,res,function(){
      auth.hasAuthorization(req,res,function(){
        notes.getNote(req.params.noteId).then(function(note){
          res.status(200).json(note);
        });
      })
    })
  })
  .delete(function (req, res) {
    notes.destroy(req.params.noteId).then(function(note){
      res.status(200).json(note);
    });
  });


  router.get('/partials/*', function(req, res) {
    var requestedView = path.join('./', req.url);
    console.log(requestedView);
    res.render(requestedView);
  });

  router.get('/*', function(req, res) {
    res.render('index');
  });

}