'use strict';

var db = require('../models'),
passport = require('passport');
/**
 * Create user
 * requires: {username, password, email}
 * returns: {email, password}
 */
 exports.create = function (req, res, next) {
  var newUser =  db.User.create(req.body).then(
    function(user) {
        req.logIn(user, function(err) {
          if (err) return next(err);
          console.log(user.user_info)
          return res.json(user.user_info);
        });
    });
};


/**
 *  Username exists
 *  returns {exists}
 */
 exports.exists = function (req, res, next) {
  db.User.findOne(  {where:  { username : req.query.username }}).then(function(user) {
    console.log(user)
    if(user) {
      res.json({exists_username: true});
    } else {
      db.User.findOne({where :{ email : req.query.email }}).then(function (user) {
        if(user) {
          res.json({exists_email: true});
        } else {
          res.json({exists_username: false,exists_email: false});
        }
      });
      
    }
  });
}
