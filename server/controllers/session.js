'use strict';

var passport = require('passport');

/**
 * Session
 * returns info on authenticated user
 */
 exports.session = function (req, res) {
  if (!req.isAuthenticated()) {
    return res.status(200).json({
      status: false
    });
  }
  res.status(200).json({
    user: req.user
  });
};

/**
 * Logout
 * returns nothing
 */
 exports.logout = function (req, res) {
  console.log("logout");
  req.logout();
  res.status(200).json({
    status: 'Bye!'
  });
};

/**
 *  Login
 *  requires: {email, password}
 * res.json(status, obj): Use res.status(status).json(obj)
 */
 exports.login = function (req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    var error = err || info;
    if (error) { 
      return res.status(400).json(error); 
    }
    req.logIn(user, function(err) {
          if (err) return next(err);
          return res.json(req.user);
        });
  })(req, res, next);
}