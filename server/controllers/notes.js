'use strict';
var db = require('../models');
var Promise = require('promise');


/**
 * Find Note by id
 */
 exports.getNote = function(id) {
  return new Promise(function(resolve,reject) {
    db.Note.findOne({ where: { id : id } }).then(function (note) {
      resolve (note)            
    }).catch(function(err){
      reject(err);
    });
  })
};

/**
 * Create a note
 */
 exports.create = function(req, res) {
  var note = db.Note.create(req.body).then(function(note){
    if(note){
      res.status(200).json(note);
    }else{
      res.status(500).json({message:"Error creating note entrance"});
    }

  });
};


/**
 * Delete a note
 */
 exports.destroy = function(id) {

  return new Promise(function(resolve,reject) {
    db.Note.findOne({ where: { id : id } }).then(function (note) {

          db.Note.findAll({ where: { subject: note.subject, UserId:note.UserId } }).then(function(notes) {
          // projects will be an array of Project instances with the specified name
          for(var i =0; i<notes.length;i++){
            var noteToMod = notes[i];
            noteToMod.status = 'deactivated'
            noteToMod.save().then(function() {
            console.log("Note with id "+noteToMod.id+" were deactivated properly");
              resolve (noteToMod);
            });
          }

        })

    }).catch(function(err){
      reject(err);
    });
  })

  
};


/**
 * List of Notes
 */
 exports.all = function(req, res) {

  db.sequelize.query('SELECT * FROM "Notes" "Note"  WHERE "Note"."id" IN (SELECT MAX("Note2"."id" ORDER BY "Note2"."version") FROM "Notes" "Note2" WHERE "Note2"."UserId" = :userId AND "Note2"."status" = :status GROUP BY "Note2"."subject");',
  { 
    replacements: { userId: req.user.id, status:'active' }, 
    type: db.sequelize.QueryTypes.SELECT,
    model: db.Note }

    ).then(function(notes) {
     res.status(200).json(notes)    })

  };

