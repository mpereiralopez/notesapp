'use strict';

//var request = require('supertest');
//var app = require('../app');

/**
 * Authenticate a test user.
 *
 * @param {User} user
 * @param {function(err:Error, token:String)} callback
 */
exports.authenticate = function (agent,user, callback) {
  agent
    .post('/auth/session/')
    .send({
      email: user.email,
      password: user.password
    })
    .end(function (err, res) {
      if (err) {
        return callback(err);
      }
      callback(null, res.body);
    });
};