var should = require('should'); 
var app = require ('../app');
var supertest = require ('supertest');
var auth_helper = require('./auth-helper');
require('should-http');

/******** URLS ***********/

var url_api_notes = '/api/notes/';



/********* Auxiliar Objects for testing purposes ************/

var userForLogin = {
	email:'pereira.lopez.1987@gmail.com',
	password : 'qwerty'
};

/******** Here we will store userId when login for future using *****/
var userIdStored = -1;

var userForLoginFake = {
	email:'fake@mail.com',
	password : 'asdfg'
};

var newNote = {
	subject : 'Mi subject from Test',
	body : 'This is the body for my test case',
	version: 1,
	UserId : -1
}


/********** Content Type ******************/
var jsonContentType = "'Content-Type', /json/";

/*********** Using agent to store sesion ******************/
var agent = supertest.agent(app);


	before(function (done) {
		auth_helper.authenticate(agent,userForLogin,function(err,body){
			userIdStored = body.id;
			done();
		});
	});


/***** Retreive the list of the notes of an user **************/
describe('Retreive list of notes for registered user', function () {

	it('should return array', function (done) {
		agent
		.get(url_api_notes)
		.expect(jsonContentType)
		.end(function(err,res){
			res.should.have.status(200);
			res.body.should.be.instanceof(Array)
			done();
		});
	});

});



/****** Create a new note ****************/
describe('Create a new note', function () {
	it('should return the new note', function (done) {
		newNote.UserId = userIdStored;
		agent
		.post(url_api_notes)
		.send(newNote)
		.expect(jsonContentType)
		.end(function(err,res){
			res.should.have.status(200);
			res.body.should.be.instanceof(Object);
			res.body.should.be.an.instanceOf(Object).and.have.property('subject', newNote.subject);
			res.body.should.be.an.instanceOf(Object).and.have.property('body', newNote.body);
			res.body.id.should.be.a.Number();
			done();
		});
	});

});