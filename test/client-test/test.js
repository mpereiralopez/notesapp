// First test for client side

var userModel = {
	id:1,
	username:'faceUser',
	password:'qwerty',
	email:'fake@mail.com'
}
var arrayErrorLogin = [{message:'Error on login, please check your credentials.'}];

var arrayNotesOfUser = [
{"id":13,
"subject":"asde123",
"body":"adasda sd ads d",
"version":1,
"status":"active",
"createdAt":"2016-12-09T10:13:56.190Z","updatedAt":"2016-12-09T10:13:56.190Z","UserId":12}
,{"id":11,"subject":"Hola Miguel","body":"Como estas? Estoy bien","version":2,"status":"active","createdAt":"2016-12-09T10:13:24.162Z","updatedAt":"2016-12-09T10:13:24.162Z","UserId":12},
{"id":14,"subject":"Mi subject from Test","body":"This is the body for my test case","version":1,"status":"active","createdAt":"2016-12-09T21:44:42.671Z","updatedAt":"2016-12-09T21:44:42.671Z","UserId":12}]



describe('Controller: LoginCtrl', function () {

  // load the controller's module
  beforeEach(module('appNotesClient'));

  var LoginCtrl,
  scope,
  location,
  $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, $controller, $rootScope, $location) {
  	$httpBackend = _$httpBackend_;
  	scope = $rootScope.$new();
  	location = $location;
  	LoginCtrl = $controller('LoginCtrl', {
  		$scope: scope
  	});


  	spyOn(location, 'path').and.returnValue('/notes');

    // mock angular form
    scope.unregisterUser = {email:'asdasd',password:'oadso'};
    //scope.optionsForm.model.$setValidity = function() {};

    // mock user
    scope.user = { email: '', password: '', username: '' };
}));

  it('should set errors on view', function () {
  	var url = '/auth/session/';
   	//scope.errors= {message:'Error on login, please check your credentials.'};
   	var httpResponse = scope.errors;
    scope.login(scope.unregisterUser); //Just moved this from after expectGET
    $httpBackend.expectPOST(url).respond(200, httpResponse);
    $httpBackend.flush();
    expect(scope.errors[0].message).toBe(arrayErrorLogin[0].message);
});

  it('should set errors on login ok and redirect', function () {
  	var url = '/auth/session/';
   	//scope.errorsc= {message:'Error on login, please check your credentials.'};
   	scope.unregisterUser = {
   		email:'fake@mail.com',password:'qwerty'
   	};
   	var httpResponse = userModel;
   	scope.login(scope.unregisterUser); 
   	$httpBackend.expectPOST(url).respond(200, userModel);
   	$httpBackend.flush();
   	expect(scope.currentUser.id).toBe(userModel.id);
   	expect(scope.currentUser.username).toBe(userModel.username);
   	expect(scope.currentUser.email).toBe(userModel.email);
   	expect(location.path).toHaveBeenCalled();
   });


});




describe('Controller: Notes Controller', function () {

  // load the controller's module
  beforeEach(module('appNotesClient'));

  var NotesCtrl,
  scope,
  location,
  _Notes,
  $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, $controller, $rootScope, $location,Notes,$routeParams) {
  	$httpBackend = _$httpBackend_;
  	scope = $rootScope.$new();
  	location = $location;
  	routeParams = $routeParams;
  	notesSpy = jasmine.createSpyObj('Notes', ['$save', 'remove', 'query','get']);

  	notesSpyFunc = function () {
  		return notesSpy
  	};

  	for(var k in notesSpy){
  		notesSpyFunc[k]=notesSpy[k];
  	}

  	NotesCtrl = $controller('NotesCtrl', {
  		$scope: scope,
  		Notes: notesSpyFunc
  	});


  	spyOn(location, 'path').and.returnValue('/notes');

    // mock autenticated user
    scope.$parent.currentUser = {id:12,username:'mpereira',email:'asdasd@mail.com',password:'oadso'};

    notesSpy.query.and.callFake(function (cb) {
    	cb(arrayNotesOfUser);
	});

	notesSpy.get.and.callFake(function (cb) {
    	scope.acutalNote = arrayNotesOfUser[0].id;
	});
    
}));

  it('should return a list of 3 notes of user', function () {
  	var url = '/api/notes';
  	var httpResponse = arrayNotesOfUser;
  	scope.currentUser = userModel;
  	
  	scope.find();
  	expect(notesSpy.query).toHaveBeenCalled();
    expect(scope.notes).toBe(arrayNotesOfUser);
});

  it('should return only the note with Id 13', function () {
  	routeParams.noteId = 13;
   	var httpResponse = arrayNotesOfUser[0];
   	scope.currentUser = userModel;
    scope.findOne();
    expect(notesSpy.get).toHaveBeenCalled();
    expect(scope.acutalNote).toBe(13);


});


});