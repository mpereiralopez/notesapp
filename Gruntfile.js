module.exports = function(grunt){
  grunt.initConfig({
    pkg:grunt.file.readJSON('package.json'),

    watch:{
      options:{livereload:true},
      files:['client/**','server/**'],
      tasks:['express:dev']
    },
    express: {
      options: {
      // Override defaults here
    },
    dev: {
      options: {
        script: 'bin/www'
      }
    }/*,
    prod: {
      options: {
        script: 'path/to/prod/server.js',
        node_env: 'production'
      }
    },
    test: {
      options: {
        script: 'path/to/test/server.js'
      }
    }*/
  },


  karma: {
    unit: {
      configFile: 'test/karma.conf.js',
     autoWatch: true
    }
  },

  mochaTest: {
    test: {
      options: {
        reporter: 'spec',
        clearRequireCache: true
      },
      src: ['test/server_test.js']
    },
  }

});

  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express-server');  
  grunt.registerTask('dev',['express','watch']);
  grunt.registerTask('karma',['karma']);
  grunt.registerTask('mocha', ['mochaTest']);


};